package shapes2d;

public class Square {
    public int edge;

    public Square(int edge) {
        this.edge = edge;
    }

    public int area(){
        return edge * edge;
    }

    @Override
    public String toString() {
        return "Square{" +
                "edge=" + edge +
                ", Area="+ area() +
                '}';
    }
}

