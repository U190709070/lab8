package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    int h;

    public Cylinder(int radius, int h) {
        super(radius);
        this.h = h;
    }

    @Override
    public double area() {
        return 2 * PI * radius * (radius + h);
    }
    public double volume(){
        return PI * radius * radius * h;
    }

    @Override
    public String toString() {
        return "Cylinder{" +
                "radius=" + radius +
                ", PI=" + PI +
                ", h=" + h +
                ", Area=" + area() +
                ", Volume"+ volume() +
                '}';
    }
}

