package shapes3d;

import shapes2d.Square;

public class Cube extends Square {
    public Cube(int edge) {
        super(edge);
    }

    @Override
    public int area() {
        return 6 * edge * edge;
    }
    public double volume(){
        return edge * edge * edge;
    }

    @Override
    public String toString() {
        return "Cube{" +
                "edge=" + edge +
                ", Area=" + area() +
                ", Volume=" + volume() +
                '}';
    }

}

