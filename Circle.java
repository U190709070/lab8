package shapes2d;

public class Circle {
    public int radius;
    public double PI =  Math.PI;

    public Circle(int radius) {
        this.radius = radius;
    }
    public double area(){
        return PI * radius * radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", PI=" + PI +
                 ", Area="+ area()+
                '}';
    }
}

